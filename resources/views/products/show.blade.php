@extends('layouts.app')

@section('content')

<h1>{{ $product->id }}</h1>
<p>{{ $product->title }}</p>
<p>{{ $product->description }}</p>
<p>{{ $product->price }}</p>
<p>{{ $product->stock }}</p>
<p>{{ $product->status }}</p>
@endsection
