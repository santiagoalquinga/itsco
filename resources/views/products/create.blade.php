@extends('layouts.app')

@section('content')

<h1>Nuevo Producto</h1>

<form method="post" action="{{ route('products.store') }}">
    @csrf
    <div class="form-row">
        <label for="">Titulo</label>
        <input class="form-control" type="text" name="title" value="{{ old('title') }}" required>
    </div>
    <div class="form-row">
        <label for="">Descripcion</label>
        <input class="form-control" type="text" name="description" value="{{ old('description') }}" required>
    </div>
    <div class="form-row">
        <label for="">Precio</label>
        <input class="form-control" type="number" min="1.00" step="0.01" name="price" value="{{ old('price') }}" required>
    </div>
    <div class="form-row">
        <label for="">Stock</label>
        <input class="form-control" type="number" min="0" name="stock" value="{{ old('stock') }}" required>
    </div>
    <div class="form-row">
        <label for="">Status</label>
        <select class="custom-select" name="status" id="" required>
            <option value="" selected>Select...</option>
            <option {{old('status') == 'available' ? 'selected' : ''}} value="available">Available</option>
            <option {{old('status') == 'unavailable' ? 'selected' : ''}} value="unavailable">Unavailable</option>
        </select>
    </div>
    <div class="form-row">
        <button type="submit" class="btn btn-primary btn-lg mt-3">Crear producto</button>
    </div>
</form>
@endsection
